//console.log("hello");

//#3-4
let numbers = 2;

let getCube = 2 ** 3
console.log(`The cube of 2 is ${getCube}`);

//#5-6
let address = [ 258, "Washington Ave", "NW", "California", 90011];

console.log(`I live at ${address[0]} ${address[1]} ${address[2]}, ${address[3]} ${address[4]}.`);

//#7-8
const animal = {
	name: "Lolong", 
	water: "saltwater", 
	weight: 1075, 
	measureFt: 20, 
	measureInch: 3}

function animalName(){
	console.log(`${animal.name} was a ${animal.water} crocodile. He weighed at ${animal.weight} kgs with a measurement of ${animal.measureFt} ft ${animal.measureInch} in.`);
}

animalName(animal);

//#9

let numbersArray = [1, 2, 3, 4, 5];

let numberEach = numbersArray.forEach((number) => {
	console.log(number); 
});

//#10

let numbersR = [1, 2, 3, 4, 5]
let iteration = 0;

let reducedNumber = numbersR.reduce((acc, cur) => {
	return acc + cur;
})
										
console.log(`Result of reduce method: ${reducedNumber}`);

//#12

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myNewDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myNewDog);
